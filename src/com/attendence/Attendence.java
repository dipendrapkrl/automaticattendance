
package com.attendence;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Attendence {
	// private String[] user_ip = new String[new
	// DatabaseOperations().noOfUsers()];
	// private int[] user_id = new int[new DatabaseOperations().noOfUsers()];
	
	
	public static final int LOGIN_TIME =1;
	public static final int LOGOUT_TIME =2;
	
	private int minLoginTimeHr ;
	private int minLoginTimeMin ;//= 0;
	
	private int maxLoginTimeHr ;//= 10;
	private int maxLoginTimeMin;// = 0;

	

	private int minLogoutTimeHr ;//= 5;
	private int minLogoutTimeMin ;//= 0;
	
	private int maxLogoutTimeHr ;//= 6;
	private int maxLogoutTimeMin ;//= 30;

	
	
	private long waitingTimeForNextPing ;//= 3; // it is in minute
	

	public void setMinLoginTimeHr(int minLoginTimeHr) {
		this.minLoginTimeHr = minLoginTimeHr;
	}

	public void setMinLoginTimeMin(int minLoginTimeMin) {
		this.minLoginTimeMin = minLoginTimeMin;
	}

	public void setMaxLoginTimeHr(int maxLoginTimeHr) {
		this.maxLoginTimeHr = maxLoginTimeHr;
	}

	public void setMaxLoginTimeMin(int maxLoginTimeMin) {
		this.maxLoginTimeMin = maxLoginTimeMin;
	}

	public void setMinLogoutTimeHr(int minLogoutTimeHr) {
		this.minLogoutTimeHr = minLogoutTimeHr;
	}

	public void setMinLogoutTimeMin(int minLogoutTimeMin) {
		this.minLogoutTimeMin = minLogoutTimeMin;
	}

	public void setMaxLogoutTimeHr(int maxLogoutTimeHr) {
		this.maxLogoutTimeHr = maxLogoutTimeHr;
	}

	public void setMaxLogoutTimeMin(int maxLogoutTimeMin) {
		this.maxLogoutTimeMin = maxLogoutTimeMin;
	}

	public void setWaitingTimeForNextPing(long waitingTimeForNextPing) {
		this.waitingTimeForNextPing = waitingTimeForNextPing;
	}

	public int getMinLoginTimeHr() {
		return minLoginTimeHr;
	}

	public int getMinLoginTimeMin() {
		return minLoginTimeMin;
	}

	public int getMaxLoginTimeHr() {
		return maxLoginTimeHr;
	}

	public int getMaxLoginTimeMin() {
		return maxLoginTimeMin;
	}

	public int getMinLogoutTimeHr() {
		return minLogoutTimeHr;
	}

	public int getMinLogoutTimeMin() {
		return minLogoutTimeMin;
	}

	public int getMaxLogoutTimeHr() {
		return maxLogoutTimeHr;
	}

	public int getMaxLogoutTimeMin() {
		return maxLogoutTimeMin;
	}

	public long getWaitingTimeForNextPing() {
		return waitingTimeForNextPing;
	}

	/**
	 * this function makes the program to sleep till the office time
	 * office time may be login time or logout time
	 * @param a 1 for login time else for logout time
	 */
	public void waitUntilOfficeTime(int a) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdfHour = new SimpleDateFormat("HH");
		SimpleDateFormat sdfMin = new SimpleDateFormat("mm");

		int arrivalHour = Integer.parseInt(sdfHour.format(cal.getTime()));
		int arrivalMin = Integer.parseInt(sdfMin.format(cal.getTime()));
		System.out.println("Current time " + arrivalHour + ":" + arrivalMin);

		if (a == LOGIN_TIME) {
			if ((arrivalHour < minLoginTimeHr)
					|| (arrivalHour == minLoginTimeHr && arrivalMin < minLoginTimeMin)) {
				long extratime = ((minLoginTimeHr - arrivalHour) * 60 + (minLoginTimeMin - arrivalMin)) * 60 * 1000;

				try {
					System.out.println("Its not time for attedence wait for "
							+ (extratime / 60000) + " minutes");
					Thread.sleep(extratime);
				} catch (InterruptedException e) {
					System.out.println("Interrupted while sleeping");
					e.printStackTrace();
				}

			}
		} else {
			if ((arrivalHour < minLogoutTimeHr)
					|| (arrivalHour == minLoginTimeHr && arrivalMin < minLogoutTimeMin)) {
				long extratime = ((minLogoutTimeHr - arrivalHour) * 60 + (minLogoutTimeMin - arrivalMin)) * 60 * 1000;

				try {
					System.out.println("Its not time for logout wait for "
							+ (extratime / 60000) + " minutes");
					Thread.sleep(extratime);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					System.out.println("Interrupted while sleeping");
					e.printStackTrace();
				}

			}
		}
	}

	/**
	 * used to determine whether the it is logout time or not
	 * @return true if its the time for possible logout
	 */
	public boolean isItLogoutTime() {

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdfHour = new SimpleDateFormat("HH");
		SimpleDateFormat sdfMin = new SimpleDateFormat("mm");

		int leavingHour = Integer.parseInt(sdfHour.format(cal.getTime()));
		int leavingMin = Integer.parseInt(sdfMin.format(cal.getTime()));
		System.out.println("Current time " + leavingHour + ":" + leavingMin);
		if ((leavingHour > minLoginTimeHr)
				|| (leavingHour == minLoginTimeHr && leavingMin >= minLoginTimeMin)) {
			
			return true;
		} else {
			return false;
		}

	}

	/**
	 * to check if there is still time for login time or logout time
	 * @param a =1 for login and else for logout
	 * @return true if successful
	 */
	public boolean isAttendanceTimeOver(int a) { // 1 for start and other for
													// end attendance
		// //this function checks whether the time upto which the attendance
		// should have been done is over or not
		// returns true if the time is over and false if time still remains
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdfHour = new SimpleDateFormat("HH");
		SimpleDateFormat sdfMin = new SimpleDateFormat("mm");
		int arrivalHour = Integer.parseInt(sdfHour.format(cal.getTime()));
		int arrivalMin = Integer.parseInt(sdfMin.format(cal.getTime()));
		if (a == LOGIN_TIME) {
			if (arrivalHour > maxLoginTimeHr) {
				System.out.println("Attendance Time Over for today1");

				return true;
			} else if (arrivalHour == maxLoginTimeHr
					&& arrivalMin >= maxLoginTimeMin) {
				System.out.println("Attendance Time Over For Today2...");

				return true;
			} else {
				System.out.println("Attendance Time: Ok ");

				return false;
			}
		} else {
			if (arrivalHour > maxLogoutTimeHr) {
				System.out.println("Attendance Time Over for today00");

				return true;
			} else if (arrivalHour == maxLogoutTimeHr
					&& arrivalMin >= maxLogoutTimeMin) {
				System.out.println("Attendance Time Over For Today00...");

				return true;
			} else {
				System.out.println("Attendance Time: Ok 00");

				return false;
			}

		}

	}

	/**
	 * pings the given ip to determine if it is available or not
	 * @param ip the IP address to ping to
	 * @return true if available else false
	 */
	public boolean ping(String ip) {
		// returns true if ping is successful and false if not successful

		System.out.println("System will now ping " + ip);
		InetAddress inet;
		try {
			inet = InetAddress.getByName(ip);
			if (inet.isReachable(5000)) {
				System.out.println("Ping Successful");
				return true;
			} else {
				System.out.println("Couldnt find " + ip);

				return false;
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		catch (IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	/**
	 * wait for specified time until next ping ie. program sleeps for 
	 * specified amount time which is set initially through database
	 */
	public void waitForPing() {
		
		try {

			long temp = waitingTimeForNextPing * 1000 * 60; 
			
			System.out.println("System will now sleep for " + (temp / 60000)
					+ " minutes");
			Thread.sleep(temp);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * this function checks if the login time is smaller than logout time 
	 * @return true if its not today
	 */
	public boolean isTodayPast() {
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdfHour = new SimpleDateFormat("HH");
		SimpleDateFormat sdfMin = new SimpleDateFormat("mm");

		int arrivalHour = Integer.parseInt(sdfHour.format(cal.getTime()));
		int arrivalMin = Integer.parseInt(sdfMin.format(cal.getTime()));
		
		if(arrivalHour>=maxLogoutTimeHr || (arrivalHour==maxLogoutTimeHr && arrivalMin>=maxLogoutTimeMin)){
			System.out.println("today past");
			return true;
		}
		System.out.println("still today");
		return false;
	}


}
