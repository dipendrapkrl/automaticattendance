package com.attendence;

public class MainProgram {

	private static int count = 0;

	public static void main(String[] args) {

		DatabaseOperations db = new DatabaseOperations();
		Attendence attend = new Attendence();

		while (true) {
			System.out
					.println("------------------------------------------------------------------------");
			if(!db.performPreviousDayForceLogout()){
					System.out.println("Failed to connect to Database");
					System.out.println("Application will now exit");// will update logout time to 00
					System.out.println("Please restart the application after fixing database issue");
					System.exit(0);
			}
												// 00 00 and adds comment to it
			System.out.println("Acquiring the office time details...");
			db.setAllParameters(attend);

			attend.waitUntilOfficeTime(1);// will holds the execution of the //1
											// for start and other for end of
											// attendance
											// program
											// until official time
			System.out.println("Attendence will now start");
			int noOfUsersToAttend = db.noOfUsers();
			System.out.println("no of users to attend " + noOfUsersToAttend);
			if (noOfUsersToAttend == 0) {
				System.out
						.println("No users in database. Please fill in the record in database  and restart the application");
				System.exit(0);
			} else {
				String[] ip = new String[noOfUsersToAttend];
				String[] arrivedIP = new String[noOfUsersToAttend];
				for (int i = 0; i < ip.length; i++) {
					ip[i] = "";
					arrivedIP[i] = "";
				}

				while ((attend.isAttendanceTimeOver(Attendence.LOGIN_TIME) == false) // 1
																						// for
																						// start
						// of
						// attendance
						// and other
						// for end
						// of
						// attendance
						&& db.getUnattendedIPs(ip,
								DatabaseOperations.ATTENDANCE_LOGIN) != 0) {

					// stops when time is over or all users arrived
					noOfUsersToAttend = db.getUnattendedIPs(ip,
							DatabaseOperations.ATTENDANCE_LOGIN);
					if (count != 0) {
						System.out.println("no of users to attend "
								+ noOfUsersToAttend);
					} else {
						System.out
								.println("following IPs will be checked for attendance "
										+ noOfUsersToAttend);
					}
					for (int i = 0; i < noOfUsersToAttend; i++) {
						if (ip[i] == null || ip[i].equals("")) // "" will be
																// filled
																// to
						{ // remove arrived
							// ips from array ip
							continue;
						}
						if (attend.ping(ip[i])) { // pings the ip and returns
													// true
													// if
													// successful
							arrivedIP[i] = ip[i];

						}
						ip[i] = "";

					}

					db.updateAttendence(arrivedIP, 1); // 1-for present and
														// 0-for
														// absent
					for (int i = 0; i < arrivedIP.length; i++) {
						arrivedIP[i] = ""; // to remove duplication and double
						ip[i] = ""; // attendence
					}
					attend.waitForPing();

				}
				System.out.println("retrieving unattended ip");
				db.getUnattendedIPs(ip, DatabaseOperations.ATTENDANCE_LOGIN); // will
																				// store
																				// unarrived
																				// IPs
				System.out.println("update attendance");
				db.updateAttendence(ip, 0); // will update their attendance to
											// absence

				for (int i = 0; i < ip.length; i++) { // to clear ip already
														// present in these
														// arrays
					ip[i] = "";
					arrivedIP[i] = "";
				}
				System.out.println("---------Login Time Over-------");
				attend.waitUntilOfficeTime(Attendence.LOGOUT_TIME); // wait until logout time
				while ((attend.isAttendanceTimeOver(Attendence.LOGOUT_TIME) == false)) {

					// stops when time is over or all users arrived
					noOfUsersToAttend = db.getUnattendedIPs(ip,
							DatabaseOperations.ATTENDANCE_LOGOUT);
					System.out.println("no of users to attend "
							+ noOfUsersToAttend);
					if (count != 0) {
						System.out.println("no of users to logout "
								+ noOfUsersToAttend);
					} else {
						System.out
								.println("following IPs will be checked for presence upto now"
										+ noOfUsersToAttend);
					}
					for (int i = 0; i < noOfUsersToAttend; i++) {
						if (ip[i] == null || ip[i].equals("")) // "" will be
																// filled
																// to
						{ // remove arrived
							// ips from array ip
							continue;
						}
						if ((attend.ping(ip[i]) == false)) { // pings the ip and
																// returns
							// true
							// if
							// successful
							arrivedIP[i] = ip[i];

						} else {
							System.out.println("arrived ip" + arrivedIP[i]);
							db.updateReAppeared(arrivedIP[i]);
						}
						ip[i] = "";

					}

					db.updateLogoutInfo(arrivedIP);
					// db.updateAttendence(arrivedIP, 1); // 1-for present and
					// 0-for
					// absent
					for (int i = 0; i < arrivedIP.length; i++) {
						arrivedIP[i] = ""; // to remove duplication and double
						ip[i] = ""; // attendence
					}
					attend.waitForPing();
				}
				db.forceLogout();


				// Make it sleep until tommorow's first hour of attendance
				int timeUntilTommorow;
				timeUntilTommorow = attend.getMinLoginTimeHr() * 60
						+ attend.getMinLoginTimeMin();
				timeUntilTommorow += 24 * 60 - attend.getMaxLogoutTimeHr() * 60
						+ attend.getMaxLogoutTimeMin();
				try {
					System.out
							.println("System will resume it's work tommorow after approx. "
									+ timeUntilTommorow / 60 + " hours");

					Thread.sleep(timeUntilTommorow * 60 * 1000);
					System.out
							.println("Back from sleep. Now will perform attendance of today");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				
				db = null;
				attend = null;

			}

		}

	}
}
