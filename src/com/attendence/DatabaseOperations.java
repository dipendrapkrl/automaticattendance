package com.attendence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseOperations {
	// private int noOfUsers =-1;
	private String driver = "com.mysql.jdbc.Driver";
	private String user = "root";
	private String pass = "root";
	private String db = "pingDb";
	private String url = "jdbc:mysql://localhost:3306/";
	private static int count = 0;
	
	public static final int ATTENDANCE_LOGIN=1;
	public static final int ATTENDANCE_LOGOUT=0;
	public static final int ATTENDANCE_NON_LOGGED=2;
	public static final int ATTENDANCE_STATUS_SET=1;
	public static final int ATTENDANCE_STATUS_UNSET=0;

	/**
	 * returns the number of users in the database
	 * @return the number of users in the database
	 *  it will return -1 if connection to database  failed
	 */
	public int noOfUsers() {
		int noOfUsers = -1;
		try {
			Class.forName(driver);
			Connection con = null;
			con = DriverManager.getConnection(url + db, user, pass);
			String sql = "select COUNT(ip_address) from users";
			PreparedStatement prest = con.prepareStatement(sql);
			ResultSet rs = prest.executeQuery();
			while (rs.next()) {
				noOfUsers = rs.getInt("COUNT(ip_address)");
			}
			con.close();
		} catch (ClassNotFoundException e) {
			System.out.println("class not found");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Sql exception");
			e.printStackTrace();
		} catch (NullPointerException e) {
			System.out.println("NUll pointer exception");
			e.printStackTrace();
		}
		System.out.println("no of users returned = "+noOfUsers);
		return noOfUsers;
	}

	

		
	/**
	 * displays the name login time and status of the user on todays date
	 * Has nothing to do with computation except to display in console
	 */
	public void getTodaysAttandance() {
		Connection con = null;
		try {
			System.out.println("Today's attendance");
			con = DriverManager.getConnection(url+db,user, pass);
			String sql = "SELECT users.name, attendence.login_time, attendence.status FROM users, attendence WHERE users.id=attendence.users_id AND attendence.attendence_date=CURDATE()";
			PreparedStatement pre = con.prepareStatement(sql);
			ResultSet rs = pre.executeQuery();
			System.out.println("Attandance for Today\n User        Login Time        status");
			while(rs.next()	){
				System.out.println(rs.getString("name")+"       "+rs.getTime("login_time")+"  "+rs.getInt("status"));
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	public int getUnattendedIPs(String[] ip, int a) {
		int i = 0; // denotes unattended number of pupils
		try {
			Connection con = null;
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			String sql;
			if(a==ATTENDANCE_LOGIN){  //for beginning attendance
			//	System.out.println("for login");
			 sql = "SELECT users.name, users.ip_address FROM users WHERE users.ip_address NOT IN (SELECT users.ip_address FROM users, attendence WHERE users.id=attendence.users_id AND attendence.attendence_date= CURDATE())";
			// this statement selects only ips of those who are not yet present
			// i.e no record of current date in attendence table
			}
			else if(a ==ATTENDANCE_LOGOUT){ //for logout attendance
				System.out.println("for logout");
				 sql ="SELECT users.name, users.ip_address FROM users, attendence WHERE users.id=attendence.users_id AND attendence.attendence_date= CURDATE() AND attendence.logout_time is null and attendence.login_time is not null";
			}
			else if( a==ATTENDANCE_NON_LOGGED){ //for gathering non logged in ips
				System.out.println("for gathering unarrived ips");
				sql ="SELECT users.name, users.ip_address FROM users, attendence WHERE users.id=attendence.users_id AND attendence.attendence_date= CURDATE() AND attendence.logout_time is null and attendence.login_time is not null";
			}else{
				sql=""; //to throw exception
			}
			PreparedStatement prest = con.prepareStatement(sql);
			ResultSet rs = prest.executeQuery();
			if(count!=0){
			System.out.println("TODO, This is left  to be implemented");
			}
			count++;
			System.out.println("Global variable  count = " +count);
			while (rs.next()) {
				
				i++;
				ip[i - 1] = rs.getString("ip_address");
				System.out.println(ip[i - 1]+""+rs.getString("name"));

			}
			
			con.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("No. of unattended ip= "+i);
		return i;

	}

	/**
	 * 
	 * @param ip the ip address of the user
	 * @return its corresponding id from the table
	 * returns -1 if failed to do so
	 */
	public int getIDFromIPAddress(String ip) {
		int id = -1;
		//System.out.println("id default= "+ id);
		try {
			Class.forName(driver);
			Connection con = null;
			con = DriverManager.getConnection(url + db, user, pass);
			String sql0 = "SELECT users.id FROM users WHERE users.ip_address=?";
			PreparedStatement pre = con.prepareStatement(sql0);
			pre.setString(1, ip);
			ResultSet rs = pre.executeQuery();
			
			rs.next();
			id = rs.getInt("id");
			System.out.println("id = "+id);
			con.close();

		} catch (ClassNotFoundException e) {
			System.out.println("exception caught while retrrieving id");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("exception caught while retrieving id");
			e.printStackTrace();
		}
		System.out.println("id returned "+ id);
		if(id==-1)System.out.println("Error Retrieving the id ");
		return id;
	}

	public boolean updateAttendence(String[] ip, int status) {

		try {
			Class.forName(driver);
			Connection con = null;
			con = DriverManager.getConnection(url + db, user, pass);
			for (int x = 0; x < ip.length; x++) {

				if (ip[x] == null || ip[x].equals("")) {

					continue;
				}
				int id=getIDFromIPAddress(ip[x]);
				String sql;
				if(status==1){
				  sql = "INSERT INTO attendence(users_id,attendence_date,STATUS,login_time) VALUES (?,CURDATE(),?, NOW())";
				}else{
				 sql = "INSERT INTO attendence(users_id,attendence_date,STATUS)VALUES (?,CURDATE(),?)";
				}
				
				PreparedStatement prest = con.prepareStatement(sql);
				prest.setInt(1, id);
				prest.setInt(2, status);
				
				System.out.println("Inserting rows");
				System.out.println("user with id " + id);

				prest.executeUpdate();

			}
			
			con.close();
			return true;

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("exception caught in update attendance");
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("exception caught in update attendance");
			e.printStackTrace();
			return false;
		}

	}
	
	/**
	 * adds logout time to the respective ip of todays date
	 * @param ip array of ip address whose logout time is to be set
	 */
	public boolean  updateLogoutInfo(String[] ip) {
		try {
			Class.forName(driver);
			Connection con = null;
			con = DriverManager.getConnection(url+db, user, pass);
			System.out.println("adding logout time of users");
			String sql= "update attendence, users set attendence.logout_time = now() where users.id=attendence.users_id and attendence.attendence_date = curdate() and users.ip_address =?";
			for(int i=0;i<ip.length;i++){
			if(ip[i]==null || ip[i].equals(""))continue;
			System.out.println("of "+ip[i] );
			PreparedStatement pre = con.prepareStatement(sql);
			pre.setString(1, ip[i]);
			pre.executeUpdate();
			System.out.println(pre.toString());
			System.out.println("done");
			
			}
			con.close();
			return true;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}





	/**
	 * adds the logout information of the user at the end of day or after the last hour of office time
	 * @return true if successful else false
	 */
	public boolean forceLogout() {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url+db, user, pass);
			System.out.println("Performing force logout");
			String sql = "update attendence set logout_time = now() where attendence_date = curdate() and status =1 and login_time is not null";
			PreparedStatement pre = con.prepareStatement(sql);
			pre.executeUpdate();
			System.out.println(pre.toString());
			System.out.println("updated");
			con.close();
			return true;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}





	/**
	 * if the server was shut down before the logout of all users were not done, it initially puts
	 * the logout information of those users 
	 * @return true if successful else false
	 */
	public boolean performPreviousDayForceLogout() {
		try {
			System.out.println("Performing yesterday's logout attendance if any(Will do only if system was off before user left the office during logout hour ");
			Class.forName(driver);
			Connection con = null;
			con = DriverManager.getConnection(url+db, user, pass);
			String sql = "UPDATE attendence SET Comments = 'Server was off before the users pc ', logout_time = '00:00:00' WHERE attendence.attendence_date= CURDATE() -1 AND login_time IS NOT NULL AND STATUS = 1 AND logout_time IS NULL ";
			PreparedStatement pre = con.prepareStatement(sql);
			pre.executeUpdate();
			System.out.println(pre.toString());
			System.out.println("updating finished");
			con.close();
			return true;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (SQLException e) {

			e.printStackTrace();
			return false;
		}
		
	}





	/**This should be called prior to calling any other function of the 
	 * attendance object
	 * reads all the attendance metadata or the parameters from the database
	 *  and assigns to the program
	 * @param attend Attendance object which should store these information
	 */
	public boolean setAllParameters(Attendence attend) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url+db, user, pass);
			String sql = "select * from info where id = 1";
			 PreparedStatement pre = con.prepareStatement(sql);
			 ResultSet res = pre.executeQuery();
			 while(res.next()){
				 System.out.println("setting office time from database");
				 attend.setMinLoginTimeHr(res.getInt("min_login_time_hr"));
				 attend.setMinLoginTimeMin(res.getInt("min_login_time_min"));
				 attend.setMaxLoginTimeHr(res.getInt("max_login_time_hr"));
				 attend.setMaxLoginTimeMin(res.getInt("max_login_time_min"));
				 
				 attend.setMinLogoutTimeHr(res.getInt("min_logout_time_hr"));
				 attend.setMinLogoutTimeMin(res.getInt("min_logout_time_min"));
				 attend.setMaxLogoutTimeHr(res.getInt("max_logout_time_hr"));
				 attend.setMaxLogoutTimeMin(res.getInt("max_logout_time_min"));
				 
				 attend.setWaitingTimeForNextPing(res.getInt("ping"));
				 
				 }
			 con.close();
			 return true;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return true;
		}
	}





	/**
	 * some user may have disconnection issue while on the office last time hour so 
	 * their logout time may be set. If those IP address re-appear, their logout time will 
	 * be cleared 
	 * @param ip ip address of the user who reappeared during logout time
	 */
	public void updateReAppeared(String ip) {
		try {
			System.out.println("updating logout time of the users who re appeared");
			Class.forName(driver);
			Connection con = null;
			con = DriverManager.getConnection(url+db, user, pass);
			String sql1 = "UPDATE attendence , users SET logout_time = NULL WHERE attendence_date = CURDATE() AND login_time IS NOT  NULL AND logout_time IS NOT NULL AND users.id=attendence.users_id AND users.ip_address=?";
				
			
			PreparedStatement pre = con.prepareStatement(sql1);
			pre.setString(1, ip);
			pre.executeUpdate();
			System.out.println("updating finished");
			con.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
	}
}
